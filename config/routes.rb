Rails.application.routes.draw do
  devise_for :users

  devise_scope :user do
    authenticated :user do
      root 'dashboards#index', as: :authenticated_root
    end

    # Go to login page if not yet logged in
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :clothes_sizes, only: [:index, :create, :update]
  resources :customers, only: [:index, :create]
  resources :design_service_price_lists, only: [:index, :show, :create, :update]
  resources :garments, only: [:index]
  resources :registrations, only: [:new, :create]
  resources :sales, only: [:index, :create]
end
