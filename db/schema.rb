# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180317071201) do

  create_table "clothes_sizes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id", null: false
    t.string "name", null: false
    t.boolean "disabled", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "fk_rails_1a1e6d28e1"
  end

  create_table "customers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "fk_rails_9917eeaf5d"
  end

  create_table "design_service_price_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "clothes_size_id", null: false
    t.bigint "created_by_id", null: false
    t.bigint "updated_by_id", null: false
    t.decimal "capital_price", precision: 10, scale: 2, null: false
    t.decimal "selling_price", precision: 10, scale: 2, null: false
    t.boolean "is_colored", default: false, null: false
    t.boolean "is_long_sleeved", default: false, null: false
    t.boolean "is_front_side", default: false, null: false
    t.boolean "is_back_side", default: false, null: false
    t.boolean "disabled", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clothes_size_id"], name: "fk_rails_4829c50cf8"
    t.index ["created_by_id"], name: "fk_rails_894d639645"
    t.index ["updated_by_id"], name: "fk_rails_49023b5d13"
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_roles_on_name"
  end

  create_table "sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "customer_id", null: false
    t.bigint "clothes_size_id", null: false
    t.bigint "created_by_id", null: false
    t.bigint "design_service_price_list_id", null: false
    t.integer "quantity", default: 1, null: false
    t.decimal "capital_price", precision: 15, scale: 2, null: false
    t.decimal "selling_price", precision: 15, scale: 2, null: false
    t.boolean "is_colored", default: false, null: false
    t.boolean "is_long_sleeved", default: false, null: false
    t.boolean "is_front_side", default: false, null: false
    t.boolean "is_back_side", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clothes_size_id"], name: "fk_rails_99aae4acc4"
    t.index ["created_by_id"], name: "fk_rails_1f545c13bf"
    t.index ["design_service_price_list_id"], name: "fk_rails_fa9e2f550a"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "role_id"
    t.string "username", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "clothes_sizes", "users"
  add_foreign_key "customers", "users"
  add_foreign_key "design_service_price_lists", "clothes_sizes"
  add_foreign_key "design_service_price_lists", "users", column: "created_by_id"
  add_foreign_key "design_service_price_lists", "users", column: "updated_by_id"
  add_foreign_key "sales", "clothes_sizes"
  add_foreign_key "sales", "design_service_price_lists"
  add_foreign_key "sales", "users", column: "created_by_id"
end
