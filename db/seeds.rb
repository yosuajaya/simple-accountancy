# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Role.count == 0
  ROLES = ['admin', 'owner']

  ROLES.each do |role|
    Role.where(name: role).first_or_create
  end
end

unless User.find_by(email: 'administrator@administrator.com')
  admin_role = Role.where(name: 'admin').first_or_create

  User.create(
    username: 'administrator',
    role: admin_role,
    email: 'administrator@administrator.com',
    password: 'administrator'
  )
end

if ClothesSize.count == 0
  admin = User.find_by(
    username: 'administrator', email: 'administrator@administrator.com',
    role: Role.admin
  )

  ClothesSize.create(user: admin, name: 'S', disabled: false)
  ClothesSize.create(user: admin, name: 'M', disabled: false)
  ClothesSize.create(user: admin, name: 'ML', disabled: false)
  ClothesSize.create(user: admin, name: 'L', disabled: false)
  ClothesSize.create(user: admin, name: 'XL', disabled: false)
  ClothesSize.create(user: admin, name: 'XXL', disabled: false)
  ClothesSize.create(user: admin, name: 'XXXL', disabled: false)
end
