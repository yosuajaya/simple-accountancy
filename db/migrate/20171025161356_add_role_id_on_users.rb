# Add role_id column on users Table
class AddRoleIdOnUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :role_id, :integer, after: :id
  end
end
