class CreateClothesSizes < ActiveRecord::Migration[5.1]
  def change
    create_table :clothes_sizes do |t|
      t.integer :user_id, limit: 8, null: false
      t.string :name, null: false
      t.boolean :disabled, default: false, null: false

      t.timestamps null: false
    end

    add_foreign_key :clothes_sizes, :users
  end
end
