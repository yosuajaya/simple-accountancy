# Create roles Table
# - admin
# - owner
class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string :name, index: true, uniqueness: true, null: false

      t.timestamps null: false
    end
  end
end
