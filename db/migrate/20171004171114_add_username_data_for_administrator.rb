# Add username data for administartor user
class AddUsernameDataForAdministrator < ActiveRecord::Migration[5.1]
  def up
    user = User.where(
      email: 'administrator@administrator.com'
    ).first_or_initialize
    user.username = 'administrator'

    user.save
  end

  def down
  end
end
