# Add username column on users Table
class AddUsernameOnUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :username, :string, null: false, after: :id
  end
end
