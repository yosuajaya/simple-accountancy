class CreateCustomer < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.integer :user_id, limit: 8, null: false
      t.string :name, null: false

      t.timestamps null: false
    end

    add_foreign_key :customers, :users
  end
end
