# Store sales history
class CreateSales < ActiveRecord::Migration[5.1]
  def change
    create_table :sales do |t|
      t.bigint :customer_id, null: false
      t.bigint :clothes_size_id, null: false
      t.bigint :created_by_id, null: false
      t.bigint :design_service_price_list_id, null: false
      t.integer :quantity, default: 1, null: false
      t.decimal :capital_price, precision: 15, scale: 2, null: false
      t.decimal :selling_price, precision: 15, scale: 2, null: false
      t.boolean :is_colored, default: false, null: false
      t.boolean :is_long_sleeved, default: false, null: false
      t.boolean :is_front_side, default: false, null: false
      t.boolean :is_back_side, default: false, null: false

      t.timestamps null: false
    end

    add_foreign_key :sales, :design_service_price_lists
    add_foreign_key :sales, :users, column: :created_by_id
    add_foreign_key :sales, :clothes_sizes
  end
end
