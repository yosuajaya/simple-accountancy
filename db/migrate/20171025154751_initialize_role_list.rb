# Add roles
class InitializeRoleList < ActiveRecord::Migration[5.1]
  ROLES = ['admin', 'owner']

  def up
    ROLES.each do |role|
      Role.where(name: role).first_or_create
    end
  end

  def down
    Role.where(name: ROLES).destroy_all
  end
end
