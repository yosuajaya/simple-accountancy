# Create design_service_price_lists table
# to store price list of design services
class CreateDesignServicePriceLists < ActiveRecord::Migration[5.1]
  def change
    create_table :design_service_price_lists do |t|
      t.integer :clothes_size_id, limit: 8, null: false
      t.integer :created_by_id, limit: 8, null: false
      t.integer :updated_by_id, limit: 8, null: false
      t.decimal :capital_price, precision: 10, scale: 2, null: false
      t.decimal :selling_price, precision: 10, scale: 2, null: false
      t.boolean :is_colored, default: false, null: false
      t.boolean :is_long_sleeved, default: false, null: false
      t.boolean :is_front_side, default: false, null: false
      t.boolean :is_back_side, default: false, null: false
      t.boolean :disabled, default: false, null: false

      t.timestamps null: false
    end

    add_foreign_key :design_service_price_lists, :clothes_sizes
    add_foreign_key :design_service_price_lists, :users, column: :created_by_id
    add_foreign_key :design_service_price_lists, :users, column: :updated_by_id
  end
end
