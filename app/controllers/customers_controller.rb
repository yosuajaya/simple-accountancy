# Handle requests for customer model
class CustomersController < ApplicationController
  def index
    @customers = Customer.all.order(created_at: :desc)
  end

  def create
    customer = Customer.new(customer_params)
    customer.user = current_user

    if customer.save
      render json: customer
    else
      render json: { errors: customer.errors.full_messages },
                   status: :not_acceptable
    end
  end

  private

  def customer_params
    params.require(:customer).permit(:name)
  end
end
