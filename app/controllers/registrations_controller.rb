# Handle registration requests
class RegistrationsController < ApplicationController
  before_action :validate_current_user

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.role = Role.owner

    if @user.save
      redirect_to new_registration_path, notice: 'Member created successfully'
    else
      redirect_to new_registration_path, warning: 'Please check again your inputs'
    end
  end

  private

  def user_params
    params.require(:user).permit(
      :username, :email, :password, :password_confirmation
    )
  end

  def validate_current_user
    return if current_user.role == Role.admin
    flash[:error] = 'You are not authorized for this page'

    redirect_to authenticated_root_path
  end
end
