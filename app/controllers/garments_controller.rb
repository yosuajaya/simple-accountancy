# Handle requests for garment model
class GarmentsSizesController < ApplicationController
  def index
    @garments = Garment.all.enabled

    respond_to do |format|
      format.json { render json: @garments }
    end
  end
end
