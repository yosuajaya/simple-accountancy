# Handle requests for sale model
class SalesController < ApplicationController
  def index
    @sales = Sale.includes(:customer, :clothes_size, :created_by).all.page(
      params[:page]
    ).per(20)
  end

  def create
    sale = Sale.new(sale_params)
    sale.created_by = current_user

    if sale.save
      result = sale.as_json
      result['customer_name'] = sale.customer_name
      result['clothes_size_name'] = sale.clothes_size_name
      result['created_by_username'] = sale.created_by_username

      render json: result
    else
      render json: { errors: sale.errors.full_messages },
                   status: :not_acceptable
    end
  end

  private

  def sale_params
    params.require(:sale).permit(
      :customer_id, :clothes_size_id, :design_service_price_list_id, :quantity
    )
  end
end
