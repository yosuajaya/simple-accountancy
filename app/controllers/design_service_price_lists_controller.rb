# Handle requests for design service price list model
class DesignServicePriceListsController < ApplicationController
  def index
    @design_service_price_list = DesignServicePriceList.includes(
      :created_by, :updated_by
    ).all
  end

  def show
    design_service_price = DesignServicePriceList.find(params[:id])
    result = design_service_price.as_json
    result['clothes_size_name'] = design_service_price.clothes_size_name

    render json: result
  end

  def create
    design_service_price = DesignServicePriceList.new(
      design_service_price_params
    )
    design_service_price.created_by = current_user
    design_service_price.updated_by = current_user

    if design_service_price.save
      result = design_service_price.as_json
      result['clothes_size_name'] = design_service_price.clothes_size_name

      render json: result
    else
      render json: { errors: design_service_price.errors.full_messages },
                   status: :not_acceptable
    end
  end

  def update
    design_service_price = DesignServicePriceList.find(params[:id])
    design_service_price.updated_by = current_user

    if design_service_price.update(design_service_price_params)
      result = design_service_price.as_json
      result['clothes_size_name'] = design_service_price.clothes_size_name

      render json: result
    else
      render json: { errors: design_service_price.errors.full_messages },
                   status: :not_acceptable
    end
  end

  private

  def design_service_price_params
    params.require(:design_service_price).permit(
      :id, :clothes_size_id, :capital_price, :selling_price, :is_colored,
      :is_long_sleeved, :is_front_side, :is_back_side, :disabled
    )
  end
end
