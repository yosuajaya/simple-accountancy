# Handle requests for size model
class ClothesSizesController < ApplicationController
  def index
    @clothes_sizes = ClothesSize.all
  end

  def create
    clothes_size = ClothesSize.new(clothes_size_params)
    clothes_size.user = current_user

    if clothes_size.save
      render json: clothes_size
    else
      render json: { errors: clothes_size.errors.full_messages },
                   status: :not_acceptable
    end
  end

  def update
    clothes_size = ClothesSize.find(params[:id])

    if clothes_size.update(clothes_size_params)
      render json: clothes_size
    else
      render json: { errors: clothes_size.errors.full_messages },
                   status: :not_acceptable
    end
  end

  private

  def clothes_size_params
    params.require(:clothes_size).permit(:name, :disabled)
  end
end
