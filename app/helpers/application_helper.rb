module ApplicationHelper
  def display_price(price)
    number_to_currency(price, unit: '', delimiter: '.', precision: 0)
  end
end
