# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Role < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  class << self
    def admin
      @admin ||= Role.find_by(name: 'admin')
    end

    def owner
      @owner ||= Role.find_by(name: 'owner')
    end
  end
end
