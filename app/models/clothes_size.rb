# == Schema Information
#
# Table name: clothes_sizes
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  name       :string(255)      not null
#  disabled   :boolean          default(FALSE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

# Store Clothes size
# e.g: S, M, L, XL, etc
class ClothesSize < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :user_id, presence: true

  scope :enabled, -> { where(disabled: false) }

  belongs_to :user
end
