# == Schema Information
#
# Table name: sales
#
#  id                           :integer          not null, primary key
#  customer_id                  :integer          not null
#  clothes_size_id              :integer          not null
#  created_by_id                :integer          not null
#  design_service_price_list_id :integer          not null
#  quantity                     :integer          default(1), not null
#  capital_price                :decimal(15, 2)   not null
#  selling_price                :decimal(15, 2)   not null
#  is_colored                   :boolean          default(FALSE), not null
#  is_long_sleeved              :boolean          default(FALSE), not null
#  is_front_side                :boolean          default(FALSE), not null
#  is_back_side                 :boolean          default(FALSE), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (clothes_size_id => clothes_sizes.id)
#  fk_rails_...  (created_by_id => users.id)
#  fk_rails_...  (design_service_price_list_id => design_service_price_lists.id)
#

# To Store sales history
class Sale < ApplicationRecord
  before_validation :initialize_data_from_design_service_price

  validates :customer_id, :design_service_price_list_id, :clothes_size_id,
            :created_by_id, :quantity, :capital_price, :selling_price,
            presence: true

  belongs_to :clothes_size
  belongs_to :customer
  belongs_to :created_by, class_name: 'User', foreign_key: :created_by_id
  belongs_to :design_service_price_list

  delegate :name, to: :clothes_size, prefix: true
  delegate :name, to: :customer, prefix: true
  delegate :username, to: :created_by, prefix: true

  private

  def initialize_data_from_design_service_price
    self.clothes_size_id = design_service_price_list.clothes_size_id
    self.capital_price = design_service_price_list.capital_price
    self.selling_price = design_service_price_list.selling_price
    self.is_colored = design_service_price_list.is_colored
    self.is_long_sleeved = design_service_price_list.is_long_sleeved
    self.is_front_side = design_service_price_list.is_front_side
    self.is_back_side = design_service_price_list.is_back_side
  end
end
