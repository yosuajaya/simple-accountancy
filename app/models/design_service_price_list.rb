# == Schema Information
#
# Table name: design_service_price_lists
#
#  id              :integer          not null, primary key
#  clothes_size_id :integer          not null
#  created_by_id   :integer          not null
#  updated_by_id   :integer          not null
#  capital_price   :decimal(10, 2)   not null
#  selling_price   :decimal(10, 2)   not null
#  is_colored      :boolean          default(FALSE), not null
#  is_long_sleeved :boolean          default(FALSE), not null
#  is_front_side   :boolean          default(FALSE), not null
#  is_back_side    :boolean          default(FALSE), not null
#  disabled        :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (clothes_size_id => clothes_sizes.id)
#  fk_rails_...  (created_by_id => users.id)
#  fk_rails_...  (updated_by_id => users.id)
#

# Store price list of design services
class DesignServicePriceList < ApplicationRecord
  validates :clothes_size_id, :created_by_id, :updated_by_id, :capital_price,
            :selling_price, presence: true

  belongs_to :clothes_size
  belongs_to :created_by, class_name: 'User', foreign_key: :created_by_id
  belongs_to :updated_by, class_name: 'User', foreign_key: :updated_by_id

  scope :enabled, -> { where(disabled: false) }

  delegate :username, to: :created_by, prefix: true
  delegate :username, to: :updated_by, prefix: true
  delegate :name, to: :clothes_size, prefix: true

  def label
    display_text = []
    long_sleeved_text = is_long_sleeved ? 'Long-Sleeved' : 'Non Long-Sleeved'

    display_text << clothes_size_name
    display_text << 'Colored' if is_colored
    display_text << long_sleeved_text

    if is_front_side && is_back_side
      display_text << '2 Sides'
    else
      display_text << '1 Side (Front)' if is_front_side
      display_text << '1 Side (Front)' if is_back_side
    end

    display_text.join(' | ')
  end
end
