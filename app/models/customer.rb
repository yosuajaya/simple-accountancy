# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  name       :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

# Store customer name
# Customer can be a person or an organization
class Customer < ApplicationRecord
  validates :user_id, presence: true
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  belongs_to :user
end
